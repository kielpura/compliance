<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'store']]);
    }


    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->guard('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function store(Request $request)
    {
        $data = [
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'first_name' => $request->first_name,
            'gender' => $request->gender,
            'last_name' => $request->last_name,
        ];
        
        $user = User::create($data);

        return response()->json(['success' => 'Account created successfuly!', 'user_id' => $user->id], 200);
    }

    public function index(){
        $user = User::all();
        return view('user', compact('users'));
        return response()->json($user);
    }

    public function update(Request $request) {
        try {
            $data = [
                'first_name' => $request->first_name,
                'gender' => $request->gender,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'contact' => $request->contact,
            ];

            if($request->password) {
                $data['password'] = Hash::make($request->password);
            }

            $book = User::find($request->id);

            $book->update($data);

           return response()->json(['message' => 'User updated successfully!'], 200);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'User not found'], 404);
        }
    }

    public function me()
    {
        return response()->json(auth('api')->user());
    }

    public function logout()
    {

        auth('api')->logout();
        return response()->json(['message' => 'User logged out successfully!']);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth('api')->user()
        ]);
    }
}
